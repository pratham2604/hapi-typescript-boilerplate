const WEB_MODULES = [{
  plugin: './../app/routes/user.js',
  routes: {
    prefix: '/api/user'
  }
}];

export default WEB_MODULES;