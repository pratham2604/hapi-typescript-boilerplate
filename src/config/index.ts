import { Server, Request, ResponseToolkit } from "@hapi/hapi";
import * as Glue from '@hapi/glue';
import Environment from '../app/libs/environment';
Environment();

import Manifest from './manifest';
const manifest = Object.assign({}, Manifest());

const composeOptions = {
  relativeTo: __dirname
};

const init = async () => {
  try {
    const server = await Glue.compose(manifest, composeOptions);
    await server.register(require('@hapi/vision'));
    server.views({
      engines: {
        html: require('handlebars')
      },
      relativeTo: __dirname,
      path: 'templates'
    });
    await server.start();
    console.log(`Server started at ${ server.info.uri }`)
  }
  catch (err) {
    console.log(err);
    console.log('Failed to start server');
    process.exit(1);
  }
};

init();