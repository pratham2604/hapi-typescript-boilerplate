import { ResponseToolkit } from "@hapi/hapi";
import * as Joi from '@hapi/joi';
import Boom from 'boom';
import async from 'async';

export = {
  auth: false,
  description: 'Get user',
  validate: {
    query: Joi.object({
      id: Joi.string().optional(),
    }),
    failAction: (request, h, err) => err
  },
  handler: async(request, h: ResponseToolkit) => {
    return new Promise((resolve, reject) => {
      const { User } = request.server.plugins.MongoDB;
      async.auto({
        users: async.asyncify(() => {
          return Promise.resolve(true);
        })
      }, (err, results) => {
        if (err) {
          console.log(err);
          return reject(err.isBoom ? err : Boom.boomify(err));
        }
        return resolve({
          data: results.users,
          success: true,
        })
      })
    })
  },
}