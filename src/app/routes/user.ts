'use strict';

exports.plugin = {  
  pkg: require('./../../../package.json'),
  name : 'user_routes',
  register: async (server, options) => {
    const Controllers = {
      index: require('./../controllers/user/index'),
      create: require('./../controllers/user/create'),
      delete: require('./../controllers/user/delete'),
      update: require('./../controllers/user/update'),
    };
    server.route([{
      method: 'GET',
      path: '/',
      config: Controllers.index,
    }, {
      method: 'POST',
      path: '/add',
      config: Controllers.create,
    }, {
      method: 'DELETE',
      path: '/delete',
      config: Controllers.delete,
    }, {
      method: 'PUT',
      path: '/update',
      config: Controllers.update,
    }]);
  }
};