const Boom = require('boom');
const _ = require('lodash')

module.exports = (models) => {
  const helper = {
    model: null,
    Find: null,
  };
  const { User } = models;
  helper.model = User;

  helper.Find = (search, throwError = true) => {
    return new Promise((resolve, reject) => {
      const promise = search.id ? User.findById(search.id) : User.find(search);
      promise.then(res => {
        if (!res || (_.isArray(res) && !res.length)) {
          if (throwError) {
            return reject(Boom.badRequest(`${User.collection.collectionName} not found`));
          }

          return resolve(false);
        }

        if (!_.isArray(res)) {
          return resolve([res]);
        }

        return resolve(res);
      }).catch(reject);
    }).then(docs => {
      if (!docs) {
        return false;
      }

      return docs;
    });
  }
  return helper;
}