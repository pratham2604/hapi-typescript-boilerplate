"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.default = {
    host: process.env.MONGO_DB_HOST,
    port: process.env.MONGO_DB_PORT,
    db: process.env.MONGO_DB_NAME,
    // authKey: process.env.RETHINK_DB_AUTH_KEY,
    // password: process.env.RETHINK_DB_AUTH_KEY,
    // user: process.env.RETHINK_DB_USER,
    modelsDir: 'app/database/models',
};
//# sourceMappingURL=options.js.map