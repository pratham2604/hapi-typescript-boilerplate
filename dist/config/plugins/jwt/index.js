"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const jwt = require("hapi-auth-jwt2");
function validate(decoded, request) {
    if (decoded.type === 'CUSTOMER' || decoded.type === 'ADMIN') {
        let data = {
            isValid: true,
            credentials: {
                scope: decoded.type,
                userId: decoded.id,
            }
        };
        return data;
    }
    return { isValid: false };
}
function register(server, options) {
    return __awaiter(this, void 0, void 0, function* () {
        yield server.register(jwt);
        yield server.auth.strategy('jwt', 'jwt', {
            key: process.env.SECRET_KEY,
            validate: validate,
            verifyOptions: { algorithms: ['HS256'] }
        });
        server.auth.default('jwt');
    });
}
exports.plugin = {
    register,
    name: 'auth-jwt'
};
//# sourceMappingURL=index.js.map