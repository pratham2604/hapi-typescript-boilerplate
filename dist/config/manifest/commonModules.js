"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const COMMON_MODULES = [{
        plugin: 'blipp',
        options: {
            showAuth: true,
            showStart: true,
            showScope: true,
        }
    }, {
        plugin: './plugins/winston',
    }, {
        plugin: './plugins/mongo',
    }, {
        plugin: './plugins/jwt',
    }];
exports.default = COMMON_MODULES;
//# sourceMappingURL=commonModules.js.map