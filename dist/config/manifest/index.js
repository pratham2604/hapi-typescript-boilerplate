"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const commonModules_1 = require("./commonModules");
const webModules_1 = require("./webModules");
function default_1() {
    const { DAEMON_OR_WEB = 'both', NODE_ENVIRONMENT, SERVER_HOST, SERVER_PORT } = process.env;
    console.log('NODE_ENVIRONMENT:' + NODE_ENVIRONMENT);
    const plugins = commonModules_1.default.concat(webModules_1.default);
    console.log('-----------------Modules------------');
    plugins.map(r => {
        console.log(r.plugin);
    });
    console.log('-----------------Modules------------');
    const manifest = {
        server: {
            port: SERVER_PORT,
            host: SERVER_HOST,
        },
        register: {
            plugins,
        }
    };
    return manifest;
}
exports.default = default_1;
//# sourceMappingURL=index.js.map