"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const Glue = require("@hapi/glue");
const environment_1 = require("../app/libs/environment");
environment_1.default();
const manifest_1 = require("./manifest");
const manifest = Object.assign({}, manifest_1.default());
const composeOptions = {
    relativeTo: __dirname
};
const init = () => __awaiter(void 0, void 0, void 0, function* () {
    try {
        const server = yield Glue.compose(manifest, composeOptions);
        yield server.register(require('@hapi/vision'));
        server.views({
            engines: {
                html: require('handlebars')
            },
            relativeTo: __dirname,
            path: 'templates'
        });
        yield server.start();
        console.log(`Server started at ${server.info.uri}`);
    }
    catch (err) {
        console.log(err);
        console.log('Failed to start server');
        process.exit(1);
    }
});
init();
//# sourceMappingURL=index.js.map