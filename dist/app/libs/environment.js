"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const Dotenv = require("dotenv");
exports.default = () => {
    const envFilePath = process.env.APP_ROOT_PATH + "/.env";
    try {
        Dotenv.config({
            path: envFilePath
        });
    }
    catch (e) {
        throw new Error("Missing .env file. please refer Readme.md");
    }
};
//# sourceMappingURL=environment.js.map