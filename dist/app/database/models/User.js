// import mongoose, { Schema, Document } from 'mongoose';
// export interface IUser extends Document {
//   email: string;
//   firstname: string;
//   lastname: string;
// }
// const UserSchema: Schema = new Schema({
//   email: {
//     type: String,
//     required: true,
//     unique: true,
//   },
//   firstname: {
//     type: String,
//     required: true,
//   },
//   lastname: {
//     type: String,
//     required: true,
//   }
// });
// export default mongoose.model<IUser>('User', UserSchema)
// import ModelHelper from '../../libs/modelHelpers';
// const { REQUIRED_STRING_SCHEMA, DEFAULT_DATE_SCHEMA } = ModelHelper;
module.exports.schema = (Schema) => {
    const User = new Schema({
        name: String,
        phone_number: {
            type: String,
            required: true,
            unique: true,
        },
        email: {
            type: String,
            required: true,
            lowercase: true,
        },
        address: String,
    }, { collection: 'User' });
    return User;
};
//# sourceMappingURL=User.js.map