'use strict';
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
exports.plugin = {
    pkg: require('./../../../package.json'),
    name: 'user_routes',
    register: (server, options) => __awaiter(void 0, void 0, void 0, function* () {
        const Controllers = {
            index: require('./../controllers/user/index'),
            create: require('./../controllers/user/create'),
            delete: require('./../controllers/user/delete'),
            update: require('./../controllers/user/update'),
        };
        server.route([{
                method: 'GET',
                path: '/',
                config: Controllers.index,
            }, {
                method: 'POST',
                path: '/add',
                config: Controllers.create,
            }, {
                method: 'DELETE',
                path: '/delete',
                config: Controllers.delete,
            }, {
                method: 'PUT',
                path: '/update',
                config: Controllers.update,
            }]);
    })
};
//# sourceMappingURL=user.js.map