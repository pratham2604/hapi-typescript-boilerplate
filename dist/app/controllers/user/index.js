"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
const Joi = require("@hapi/joi");
const boom_1 = require("boom");
const async_1 = require("async");
module.exports = {
    auth: false,
    description: 'Get user',
    validate: {
        query: Joi.object({
            id: Joi.string().optional(),
        }),
        failAction: (request, h, err) => err
    },
    handler: (request, h) => __awaiter(void 0, void 0, void 0, function* () {
        return new Promise((resolve, reject) => {
            const { User } = request.server.plugins.MongoDB;
            async_1.default.auto({
                users: async_1.default.asyncify(() => {
                    return Promise.resolve(true);
                })
            }, (err, results) => {
                if (err) {
                    console.log(err);
                    return reject(err.isBoom ? err : boom_1.default.boomify(err));
                }
                return resolve({
                    data: results.users,
                    success: true,
                });
            });
        });
    }),
};
//# sourceMappingURL=index.js.map